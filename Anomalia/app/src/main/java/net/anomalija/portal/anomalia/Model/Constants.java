package net.anomalija.portal.anomalia.Model;


public class Constants {
//    localbase url, use only for local test
//    public static String baseUrl = "http://192.168.0.105/";
    public static String BASE_URL = "https://www.anomalija.net/dPs4f6SgJN/";
    public static final String GET_NEWS = BASE_URL + "json.php";
    public static final String GETTING_CONTENT =  BASE_URL + "getting_content.php?id=";
    public static final String GETTING_NEWS_FROM_ID = BASE_URL + "getNewsById.php?id=";
    public static final String GETTING_CATEGORY = BASE_URL + "getting_category.php";
    public static final String GETTING_CATEGORY_FROM_POST = BASE_URL + "getting_category_for_post.php?id=";
    public static final String GETTING_POSTS_FOR_CATEGORY = BASE_URL + "geting_posts_for_category.php?id=";
    public static final String REGISTRE = BASE_URL + "register.php";
    public static final String ERROR = "Connected faild";
}
